# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import subprocess
import json
import time
def getDatabase():
    try:
        dataFile = open('data.json','r')
        data = json.loads(dataFile.read())
        dataFile.close()
        return data
    except IOError:
        data = json.loads('{"data":[]}')
        return data
def saveDatabase(database):
    dataFile = open('data.json','w')
    dataFile.write(json.dumps(database))
    dataFile.close()
def updateDatabase(database, down, up):
    database["data"].append({"time":int(time.time()), "down":down,"up":up})
def getSpeed():
	p = subprocess.Popen(['speedtest-cli','--simple'], stdout=subprocess.PIPE,stderr=subprocess.PIPE)
	out, err = p.communicate()
        down = out.split("Download: ")[1].split(" Mbits/s")[0]
        up = out.split("Upload: ")[1].split(" Mbits/s")[0]
        database = getDatabase()
        updateDatabase(database, down, up)
        saveDatabase(database)
getSpeed()
