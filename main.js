function generateData(){
    var data1 = []
    var data2 = []
    $.ajax({
        url: "data.json",
        dataType: 'json',
        async: false,
        success: 
        function(json) {
            for (var i in json['data']){
                data1.push([(json['data'][i]['time']*1000),parseInt(json['data'][i]['down'])])
            }
            for (var i in json['data']){
                data2.push([(json['data'][i]['time']*1000),parseInt(json['data'][i]['up'])])
            }
        }});
    return [data1,data2];
}
function runPlot(data1,data2){
    //console.log([{label:'test', data:data}])
    
    $.plot($("#placeholder"), [data1,data2], {
        xaxis: {mode: "time"},
        series: {
            points: { show: true },
            lines: { show: true },
        }
        });
    //$.plot($("#placeholder"), $.plot($("#placeholder"), [ [10, 1], [17, -14], [30, 5] ]));
    //var plot = $("#placeholder").plot({label:'test', data:data}).data("plot");
}
function main(){
    var data = generateData();
    //console.log(data[0])
    runPlot(data[0],data[1]);
}
$(document).ready(main);